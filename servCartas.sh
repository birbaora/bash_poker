#!/bin/bash
# carga el estado de la partida (o crea una nueva con todas las cartas en la baraja)
# servidor de Cartas, entrega dos cartas aleatorias al clietne que se conecte
# guarda el nuevo estado de la partida... pensando en que volverá a ser llamado para
# otro jugador nuevo.

function mostrarBaraja(){
	indice=0;
	echo  "--------------- Estado cartas ------------------"
# anteponer '\033[0m' parpadeo
	while [ $indice -lt 52 ] ; do
		resto=$( expr $indice % 13 )
		if [ $resto -eq 0 ] ; then
			echo ""
			palo=$( expr $indice / 13 | tr 0123 tdpc)
			echo -e -n "$palo    "
		fi
		case ${baraja[$indice]} in
		  b )
			echo -e -n "${baraja[$indice]}"
				;;
		  m )
			echo -e -n '\033[0;101m''\033[1;34m'"${baraja[$indice]}"
				;;
				
		  [0-9] ) 
			echo -e -n '\033[0;103m''\033[1;31m''\033[5m''\033[4m'"${baraja[$indice]}"
				;;
		  * ) 
			echo -e -n "${baraja[$indice]}"
   		    	
	        esac
		#reset format
		echo -e -n '\033[0m'"  "
		(( indice++ ))
	done
	echo -e -n '\033[0m'"\n----------------------------------------------\n"
}


function obtenerCarta(){
# recibe un número devuelve una carta al estilo "4p" 
	palo=$( expr  $1 / 13 | tr 0123 tdpc )

	resto=$[ $1 % 13 ]
	case $resto in 
	"0") 	
		carta=A
		;;
	"10")
		carta=J
		;;
	"11")	
		carta=K
		;;
	"12")	
		carta=Q
		;;
	*) 
		carta=$[ $resto + 1 ]
 esac
 echo ${carta}${palo}
}


function inicializarBaraja(){
	#Inicializar la baraja entera ubicando todos los elementos (o cartas) en
	# la baraja
	i=0;
	while [ $i -lt 52 ] ; do
		baraja[$i]="b"
		(( i++ ))
	done
}


function guardarPartida(){

	rm -f ficheroPartida

	echo "${baraja[@]}" > ficheroPartida
}

function cargarPartida() {
	# me cargo el puto vector por si está contaminado.
	unset 'baraja[@]'
	lineaBaraja=$( head -1 ficheroPartida )
	indice=0
	for carta in $lineaBaraja ;  do
		baraja[$indice]=$carta
		(( indice++ ))
	done
}

######################################################
# Aquí comienza la mandinga
######################################################

# ¿ Hay un fichero de la partida que está en marcha ?
if [ ! -f ficheroPartida ] ; then
	inicializarBaraja
else
	cargarPartida
fi

#guardarPartida
#inicializarBaraja
#cargarPartida

#mostrarBaraja

function sacaCarta(){
	local destino=$1

	pos=$( expr $RANDOM % 52 )
	while [ ${baraja[$pos]} != "b" ] ; do
		pos=$( expr $RANDOM % 52 )
	done

	echo $pos
}

una=$( sacaCarta )
baraja[$una]=1
dos=$( sacaCarta )
baraja[$dos]=1


carta1=$( obtenerCarta $una )
carta2=$( obtenerCarta $dos )

echo "$carta1 $carta2"


#al final guardo la partida.
guardarPartida





