#!/bin/bash
# carga el estado de la partida (o crea una nueva con todas las cartas en la baraja)
# servidor de Cartas, entrega dos cartas aleatorias al clietne que se conecte
# guarda el nuevo estado de la partida... pensando en que volverá a ser llamado para
# otro jugador nuevo.
rondaFinalizada=false;

function mostrarBaraja(){
	indice=0;
	echo  "--------------- Estado cartas ------------------"
# anteponer '\033[0m' parpadeo
	while [ $indice -lt 52 ] ; do
		resto=$( expr $indice % 13 )
		if [ $resto -eq 0 ] ; then
			echo ""
			palo=$( expr $indice / 13 | tr 0123 tdpc)
			echo -e -n "$palo    "
		fi
		case ${baraja[$indice]} in
		  b )
			echo -e -n "${baraja[$indice]}"
				;;
		  m )
			echo -e -n '\033[0;101m''\033[1;34m'"${baraja[$indice]}"
				;;
				
		  [0-9] ) 
			echo -e -n '\033[0;103m''\033[1;31m''\033[5m''\033[4m'"${baraja[$indice]}"
				;;
		  * ) 
			echo -e -n "${baraja[$indice]}"
   		    	
	        esac
		#reset format
		echo -e -n '\033[0m'"  "
		(( indice++ ))
	done
	echo -e -n '\033[0m'"\n----------------------------------------------\n"
}


function obtenerCarta(){
# recibe un número devuelve una carta al estilo "4p" 
	palo=$( expr  $1 / 13 | tr 0123 tdpc )

	resto=$[ $1 % 13 ]
	case $resto in 
	"0") 	
		carta=A
		;;
	"10")
		carta=J
		;;
	"11")	
		carta=K
		;;
	"12")	
		carta=Q
		;;
	*) 
		carta=$[ $resto + 1 ]
 esac
 echo ${carta}${palo}
}


function inicializarBaraja(){
	#Inicializar la baraja entera ubicando todos los elementos (o cartas) en
	# la baraja
	i=0;
	while [ $i -lt 52 ] ; do
		baraja[$i]="b"
		(( i++ ))
	done
}


function guardarPartida(){

	rm -f ficheroPartida

	echo "${baraja[@]}" > ficheroPartida
}

function cargarPartida() {
	unset 'nombres[@]'
	unset 'pilas[@]'
	viene=nombre
	lineaJugadores=$( head -1 ficheroPartida )
	i=0
	for palabra in $lineaJugadores ;  do
		if [ $viene == nombre ] ; then
			nombres[$i]=$palabra
		fi 
		
		if [ $viene == cantidad ] ; then
			pilas[$i]=$palabra
			(( i++ ))
		fi

		if [ $viene == nombre ] ; then
        		viene=cantidad
    		else
        		viene=nombre
    fi

	done

	# me cargo el puto vector por si está contaminado.
	unset 'baraja[@]'
	lineaBaraja=$( head -2 ficheroPartida | tail -1 )
	indice=0
	for carta in $lineaBaraja ;  do
		baraja[$indice]=$carta
		(( indice++ ))
	done
}


function sacaCarta(){
	local destino=$1

	pos=$( expr $RANDOM % 52 )
	while [ "${baraja[$pos]}" != "b" ] ; do
		pos=$( expr $RANDOM % 52 )
	done

	echo $pos
}

################ comprobarBaraja ######################################
#esta función comprueba que no existan jugadores con más de dos cartas o con 
#una sola carta. Deben tener 0 o dos cartas
function comprobarBaraja(){
 	unset 'contador[@]'	

#suponiendo que llega la línea del fichero
#b b b b b b b b b 1 b b b b b b 2 b b 1 b 2 b b b b 1 b b b 
# el siguiente bucle sólo hace el vector de contadores. NO COMPRUEBA .
# eso vendrá después, cuando 

	for carta in $@ ; do
		if echo $carta | egrep 	'^[0-9]+$' > /dev/null; then
#			if  echo "${contador[$carta]}" | egrep '^[0-9]+$' ; then
			if [ -n "${contador[$carta]}" ] ; then
				contador[$carta]=$[ ${contador[$carta]} + 1 ] 
			else
			     # es la primera carta de este jugador. no existe contador
			     # para él. hay que crear por primera vez 
			     contador[$carta]=1;
			fi
		fi
	done

	## ya tenemos en el vector cuántas cartas tiene cada jugador. hay que repasar
	## sus valores y verificar que son 0 o 2. 
	i=0;
	while [ $i -lt 9 ] ; do
		if [ -n "${contador[$i]}" ] && [ "${contador[$i]}" -ne 2 ] ; then
			echo "ERROR contador[$i]=${contador[$i]}" 
			return 1
		fi
		(( i++ ))
	done
	return 0;
}

#versión Hacker
function comprobarBarajaH(){
	
	numJugador=0;
	while [ $numJugador -lt 9 ] ; do
		veces=$( echo $@ | grep -o $numJugador | wc -l )
		if ! ( [ $veces -eq 0 ] || [ $veces -eq 2 ]  ) ; then
			return 1; 
		fi
		(( numJugador++ ))
	done
	return 0;

}

# Esta función obtiene el número del jugador que tiene el turno. 
# en la línea 0 f 1 100 2 v 3 t 4 v      se quedaría con el núm 3
function jugadorTurno(){
	#me aprovecho vilmente del formato de la línea: 0 es el jugador primero
	anterior=0

	jugadorAnterior="error"  # de momento sospecho que no es
	for palabra in $@ ; do

		if [ "$palabra" == "t" ] ; then
			jugadorAnterior=$anterior
		fi
		anterior=$palabra
	done

	if [ $jugadorAnterior == "error" ] ; then
		echo "Error en partida " >&2
	fi
	
	echo "$jugadorAnterior"

}

######################################################
# Esta función crea el vector de situación a partir de una línea 
# como 0 f 1 100 2 v 3 t 4 v el vector será usado deespués.
# El vector es una vble Global! por lo cual habremos de confesarnos ante el cura
######################################################
function construirVectorSituacion(){
	i=0;
	apuesta=false;
	for dato in $@ ; do
		if [ $apuesta == true ] ; then
			situacion[$i]=$dato
			(( i++ ))
			apuesta=false
		else
			apuesta=true;
		fi
	done

}


######################################################
# Función auxiliar que encuentra la apuesta máxima
######################################################
function maximoSituacion(){
	i=0;
	tamanyo=${#situacion[@]}

	maximo=0
	while [ $i -lt $tamanyo ] ; do

		if [ ${situacion[$i]} -gt $maximo ] 2>/dev/null ; then
			maximo=${situacion[$i]}
		fi
		(( i++ ))
	done
	
	echo $maximo

}

######################################################
# Esta función realiza cambios en el estado de la partida en base a la 
# decisión tomada por un jugador en si "va", "no va" o el cabrón "sube la
# apuesta"
######################################################

function ejecutarApuesta(){
    comando=$1
    cantidad=$2
    cadena=$( head -3 ficheroPartida | tail -1 )
#supongo que todo está bien y eljugador del comando es el que tiene el turno
    iJugadorTurno=$( jugadorTurno $cadena )
    tamanyo=${#situacion[@]} 

	max=$( maximoSituacion )  

# 1o Actualizar la información del jugador del comando   
    case $comando in
        "va" )
            situacion[$iJugadorTurno]=v
            ;;
        "nova" )
            situacion[$iJugadorTurno]=f
            ;;
        "sube" )
		if [ $max -le $cantidad] ; then
			return 1;
		fi
            situacion[$iJugadorTurno]=$cantidad
		## habremos de hacer return en caso de mala apuesta
		max=$( maximoSituacion )  
            ;;
    esac

#2o Pasar el turno a otro jugador. Buscarlo "rotativamente"

	i=$[ $iJugadorTurno + 1 ]
        i=$[ $i % $tamanyo ]

	

# me preparo un bucle por si me cuesta encontrar un jugador 
	while [ true ] ;  do
    		if [ ${situacion[$i]} == "v" ] ; then
       		 	situacion[$i]=t
	        	break; # mejor un puto return 0;
    		fi
# a partir de aquí el jugador no tendrá una "v" en su situacion
   	 	if [ ${situacion[$i]} == "f" ] ; then
    		   test true # no hay que hacer nada, pero al menos ponemos algo
    		else #es un número segurísimo (salvo error en fichero)
		# 	max=$( maximoSituacion )    //ahora lo hago arriba
		#echo "ejecutarApuesta() situacion[$i]=${situacion[$i]} y max = $max ">&2
			if [ ${situacion[$i]} -eq $max ] ; then
				situacion[$i]="v"
				rondaFinalizada=true
				apuestaRonda=$max
			else 
				situacion[$i]="t"
			fi
			break		
		fi
	#no debo haber encontrado nada, avanzo al siguiente jugador.
     		(( i++ ))
        	i=$[ $i % $tamanyo ]
done
}

######################################################
# totalFichas()
######################################################

function totalFichas() {
	i=0
	while read linea ; do

	    if [ $i -gt 2 ] ; then
		#aquí procesamos la línea
		local totalLinea=0;
		local primera=true
		local jugadores=0
		for dato in $linea ; do
			if [ $primera == true ] ; then
				primera=false
				fichas=$dato
			else
				((jugadores++)) #es lo mateix que j = j + 1
			fi
		done
		totalLinea=$[ $fichas * $jugadores ]
	    	acumulado=$[ $acumulado + $totalLinea ]
	    fi
	
	    (( i++ ))
	done < ficheroPartida

	echo $acumulado
}

function salvarPartida() {

	rm -rf ficheroPartida.temp
	i=0;
	while [ $i -lt ${#nombres[@]} ] ; do
		echo -n "${nombres[$i]}"  >> ficheroPartida.temp
		echo -n "${pilas[$i]} " >>	ficheroPartida.temp
		(( i++ ))
	done > ficheroPartida.temp
	echo  "" >> ficheroPartida.temp

	echo "${baraja[@]}" >> ficheroPartida.temp

	echo "${situacion[@]}" >> ficheroPartida.temp

	tail +4 ficheroPartida >> ficheroPartida.temp
	
	if [ $rondaFinalizada == true ] ; then
		echo -n $apuestaRonda >> ficheroPartida.temp 
		i=0;
		while [ $i -lt ${#situacion[@]} ] ; do
			if [ ${situacion[$i]} == "v" ] ; then
				echo -n " $i " >> ficheroPartida.temp
			fi
			(( i++ )) 
		done
	fi
	echo "" >>ficheroPartida.temp


}

######################################################
# Aquí comienza la mandinga
######################################################

# ¿ Hay un fichero de la partida que está en marcha ?
if [ ! -f ficheroPartida ] ; then
	inicializarBaraja
else
	cargarPartida
fi

if  ! comprobarBaraja ${baraja[@]}  ; then
	echo "Hay un error en el fichero de la partida" >&2
else 
	echo "Fichero de partida OK" >&2
fi



echo baraja=${baraja[@]}
echo "jugadores = ${nombres[@]}"
echo "pilas = ${pilas[@]}"


una=$( sacaCarta )
baraja[$una]=1
dos=$( sacaCarta )
baraja[$dos]=1


carta1=$( obtenerCarta $una )
carta2=$( obtenerCarta $dos )

echo "$carta1 $carta2"


#al final guardo la partida.
#guardarPartida


#### PRUEBA
# leeré un comando y veré si el jugador que lo lanza es el que tiene elturno
# (el turno está almacenado en el fichero de la partida 
read -p "dame el comando " leido
#leido="Jugador 4 va"

#cadena="0 f 1 100 2 v 3 t 4 v 5 f"
cadena=$( head -3 ficheroPartida | tail -1 )
numJugadorCadena=$( jugadorTurno $cadena )

#desmigo la cadena leída en las partes que indican acciones, jugador y cantidad.
numJugadorComando=$( echo $leido | cut -d" " -f2)
cantidad=$( echo $leido | cut -d" " -f4)
comando=$( echo $leido | cut -d" " -f3)


if [ $numJugadorCadena -ne $numJugadorComando ] ; then
	echo "calla capullo, que no tienes el puto turno"
else
	echo "el jugadoro $numJugadorCadena ha lanzado el comando $comando y es ok"
fi

#primera misión: Formar el vector "situacion" a partir de la línea del fichero 
#para describir la situación de cada jugador.

construirVectorSituacion $cadena

echo situacion=${situacion[@]} >&2

ejecutarApuesta "$comando" $cantidad

echo situacion=${situacion[@]} >&2

echo "=========== Fichas ganadas por el jugador "

totalFichas


salvarPartida
# resultado de tu algoritmo con el ejemplo anterior es : jugadorTurno-> " 3 "




