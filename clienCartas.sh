#!/bin/bash


function mostrarMano(){
	una=$( echo $1 | tr c h | tr t c | tr p s | tr [a-z] [A-Z] )
	dos=$( echo $2 | tr c h | tr t c | tr p s | tr [a-z] [A-Z] )

	eog "imagenes/$una.png"&
	eog "imagenes/$dos.png"&
	
}


# cartas recibirá el mensaje del servidor. Se esperan dos cartas en la misma línea
# cartas valdrá algo así como "4p 2c"
read cartas

echo ok

mostrarMano $cartas

exec 1>&-


