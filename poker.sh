#!/bin/bash

#formato de la mano del jugador
#carta está compuesta de "numero o figura" seguido de "palo"
#números posibles: A,2,3,4,5,6,7,8,9,10,j,k,q    palos posibles: c,d,t,p,

#mano="Ac 4t"

function comprobarCarta(){
	#estudiar si es un 10 o es otra carta.	
	long=$( echo -n $1 | wc -m )
	if [ $long -eq 2 ] ; then
		carta=$( echo $1 | egrep '^[aA23456789jJkKqQ][pdctPCDT]$' ) 
	else #carta diez
		carta=$( echo $1 | egrep '^10[pdctPCDT]$' ) 
	fi

	if [ "$carta" != $1 ] ; then
		return 1;
	fi
	return 0;
}

function comprobarMano(){
	#exigimos dos argumentos (dos cartas)
	if [ $# -ne 2 ] ; then return 1; fi

	if comprobarCarta $1 && comprobarCarta $2 ; then
		return 0;
	else
		return 1;
	fi
}

function mostrarMano(){
	una=$( echo $1 | tr c h | tr t c | tr p s | tr [a-z] [A-Z] )
	dos=$( echo $2 | tr c h | tr t c | tr p s | tr [a-z] [A-Z] )

	eog "imagenes/$una.png"&
	eog "imagenes/$dos.png"&
	
}

mostrarMano 3c 4p
mano[0]="Ac 4t"
mano[1]="11c 4t"
mano[2]="10a 4t"
mano[3]="Ac Ap"
mano[4]="Ac Ap 4t"
mano[5]="5c 9x"

cuenta=0;
while [ $cuenta -lt ${#mano[@]} ] ; do
	if comprobarMano ${mano[$cuenta]} ; then
		echo "la mano ${mano[$cuenta]} es correcta"
	else
		echo "la mano ${mano[$cuenta]} es incorrecta"
	fi
	((cuenta++))
done


mano="Ac 4t"

if comprobarMano $mano ; then
	echo "la mano $mano es correcta"
fi

